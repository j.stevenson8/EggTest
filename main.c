#include "header.h"
//In the main program we ask the user for the number of floors that they want to test with and the number of eggs.Then the program will print to the screen the minimum guaranteed-sufficient trials required.
void main()
{
//"input" will be where we store the number of floors and eggs that the user wants to test. Then we will store the number of floors in "floors" and the number of eggs in "eggs".
        char input[100];
        int floors,eggs;

        printf("Number of floors: ");
        fgets(input,100,stdin);
        sscanf(input, "%d", &floors);

        printf("Number of eggs: ");
        fgets(input,100,stdin);
        sscanf(input, "%d", &eggs);
//We will get the number of trails from the "egg" function.
        printf("Minimum guaranteed-sufficient trials required: %d\n", egg(floors,eggs));
        return;
}




