CCOMP = gcc
COMPILEFLAGS = -g

eggtest: main.o egg2.o max.o header.h
	$(CCOMP) $(COMPILEFLAGS) -o eggtest main.o egg2.o max.o

main.o: main.c egg2.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c main.c egg2.c 

egg2.o: egg2.c max.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c egg2.c max.c

max.o: max.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c max.c 

clean: 
	rm main.o max.o egg2.o eggtest

