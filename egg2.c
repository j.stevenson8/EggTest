#include "header.h"
//"saves" will store the number of floors and eggs.
int saves [500][500]={0};
//This function will determine how many trails will be required to answer the question: What is the hightest floor from which an egg can be dropped without breaking? It will take in the number of floors and eggs that the user specified.
int egg(int floors, int eggs)
{
//"test" will be the number of required trails.
 int tests;
 int f;
//This if statement will return a zero if there are no eggs.
 if(eggs == 0)
 {
        return 0;
 }
//This if statement will return a zero if the floors the user enters is zero or less.
 if(floors <= 0)
 {
        return 0;
 }
//This if statement will return an one if the floors the user enters is one.
 if(floors == 1)
 {
        return 1;
 }
//This if statement will return the number of floors the user enters if there is only one egg.
 if(eggs == 1)
 {
        return floors;
 }

//This if statement will recursively check "saves" to see if it is not zero. It will return the number of eggs for each floor.
 if(saves[floors][eggs] != 0)
 {
        return saves[floors][eggs];
 }
//"f" will be the number of floors plus one then divided my two.
 f=((floors+1)/2);
//"tests" is the minimun number of trails required. It will call the function max.
 tests=1+max(egg(f-1, eggs-1), egg(floors-f, eggs-1));
//This will set an index of eggs to test.
 saves[floors][eggs]=tests;
 return tests;
}

